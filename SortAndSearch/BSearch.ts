let instance: BSearch;

export class BSearch {

    private operations: number = 0;

    private searchSingle(array: number[], searchedNumber: number, min: number, max: number): number {
        this.operations++;

        let index: number = Math.floor(min + ((max - min) / 2));

        if (searchedNumber === array[index]) {
            return index + 1;
        }

        if (index === min) {
            return -1;
        }

        if (searchedNumber > array[index]) {
            return this.searchSingle(array, searchedNumber, index, array.length);
        }

        if (searchedNumber < array[index]) {
            return this.searchSingle(array, searchedNumber, min, index);
        }
    }

    search(array: number[], searchedNumbers: number[]): number[] {
        let indexes: number[] = [];

        for (let i: number = 0; i < searchedNumbers.length; i++) {
            indexes[i] = this.searchSingle(array, searchedNumbers[i], 0, array.length);
        }

        return indexes;
    }

    getOperations(): number {
        return this.operations;
    }

    static instance(): BSearch {
        return instance || (instance = new BSearch())
    }
}
