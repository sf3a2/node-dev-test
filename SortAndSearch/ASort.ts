export class ASort {
    // https://en.wikipedia.org/wiki/Insertion_sort
    static sort(array: number[]): number[] {
        let i: number = 1;
        let j: number = 0;
        let x: number = 0;

        while (i < array.length) {
            j = i - 1;
            x = array[i];
            while (j >= 0 && array[j] > x) {
                array[j + 1] = array[i];
                j--
            }
            array[j + 1] = x;
            i++;
        }

        return array;
    }
}
