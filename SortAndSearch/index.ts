import {ASort} from './ASort';
import {BSort} from './BSort';
import {BSearch} from './BSearch';

const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
const unsorted2 = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];

const elementsToFind = [1, 5, 13, 27, 77];

const searchResults = BSearch.instance().search(BSort.sort(unsorted2), elementsToFind);

console.log('ASort result: ' + JSON.stringify(ASort.sort(unsorted)));
console.log('BSort result: ' + JSON.stringify(BSort.sort(unsorted2)));
console.log('Binary search results: ' + JSON.stringify(searchResults));
console.log('Binary search number of operations: ', BSearch.instance().getOperations());

