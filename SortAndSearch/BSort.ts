export class BSort {
    // https://en.wikipedia.org/wiki/Counting_sort
    static sort(array: number[]): number[] {

        let count: number[] = [];
        let output: number[] = [];
        let max = Math.max(...array) + 1;

        for (let i: number = 0; i < max; i++) {
            count[i] = 0;
        }

        for (let i: number = 0; i < array.length; i++) {
            count[array[i]]++
        }

        for (let i: number = 1; i < max; i++) {
            count[i] += count[i - 1]
        }

        for (let i: number = array.length - 1; i >= 0; i--) {
            output[--count[array[i]]] = array[i];
        }

        return output;
    }
}
