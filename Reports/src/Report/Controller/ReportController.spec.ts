import {Test} from '@nestjs/testing';
import {ReportController} from './ReportController';
import {OrderMapper} from '../../Order/Service/OrderMapper';
import {OrderModule} from '../../Order/OrderModule';

describe('reportController', () => {
    let reportController: ReportController;
    let orderMapper: OrderMapper;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            imports: [OrderModule],
            controllers: [ReportController],
            providers: [],
        }).compile();

        orderMapper = module.get<OrderMapper>(OrderMapper);
        reportController = module.get<ReportController>(ReportController);
    });

    describe('getBestsellerByDate', () => {
        it('should return a best selling product', async () => {
            const result = {
                productName: 'Black sport shoes',
                quantity: 2,
                totalPrice: 220,
            };
            jest.spyOn(orderMapper, 'getBestsellerByDate').mockImplementation(() => Promise.resolve(result));

            expect(await reportController.bestSellers('2019-08-07')).toBe(result);
        });
    });

    describe('getBestBuyersByDate', () => {
        it('should return a list of best buyers sorted descending by total spent', async () => {
            const result = [
                {
                    total: 135.75,
                    fullName: 'John Doe',
                },
                {
                    total: 110,
                    fullName: 'Jane Doe',
                },
            ];
            jest.spyOn(orderMapper, 'getBestBuyersByDate').mockImplementation(() => Promise.resolve(result));

            expect(await reportController.bestBuyers('2019-08-07')).toBe(result);
        });
    });
});
