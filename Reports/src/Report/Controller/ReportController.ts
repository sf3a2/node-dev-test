import {Controller, forwardRef, Get, Inject, Param} from '@nestjs/common';
import {IBestBuyers, IBestSellers} from '../Model/IReports';
import {OrderMapper} from '../../Order/Service/OrderMapper';

@Controller()
export class ReportController {
    constructor(
        @Inject(forwardRef(() => OrderMapper))
        private readonly orderMapper: OrderMapper,
    ) {
    }

    @Get('/report/product/:date')
    async bestSellers(@Param('date') date: string): Promise<IBestSellers> {
        return await this.orderMapper.getBestsellerByDate(date);
    }

    @Get('/report/customers/:date')
    async bestBuyers(@Param('date') date: string): Promise<IBestBuyers[]> {
        return this.orderMapper.getBestBuyersByDate(date);
    }
}
