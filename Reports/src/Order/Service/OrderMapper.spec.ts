import {Test, TestingModule} from '@nestjs/testing';
import {OrderMapper} from './OrderMapper';
import {Repository} from './Repository';
import {Order} from '../Model/Order';
import {Customer} from '../Model/Customer';
import {Product} from '../Model/Product';

describe('OrderMapper', () => {
    let repository: Repository;
    let mapper: OrderMapper;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [Repository, OrderMapper],
        }).compile();

        repository = module.get<Repository>(Repository);
        mapper = module.get<OrderMapper>(OrderMapper);
        jest.spyOn(repository, 'fetchOrders')
            .mockImplementation(() => Promise.resolve([
                {
                    number: '1970/01/1',
                    customer: 1,
                    createdAt: '1970-01-01',
                    products: [1],
                },
            ]));
        jest.spyOn(repository, 'fetchProducts')
            .mockImplementation(() => Promise.resolve([
                {id: 1, name: 'Test', price: 1},
            ]));
        jest.spyOn(repository, 'fetchCustomers')
            .mockImplementation(() => Promise.resolve([
                {
                    id: 1,
                    firstName: 'A',
                    lastName: 'B',
                },
            ]));
    });

    it('should return an array of orders', async () => {
        const expectedArray = [
            new Order({
                customer: new Customer({
                    firstName: 'A',
                    lastName: 'B',
                }),
                number: '1970/01/1',
                products: [
                    new Product({
                        name: 'Test',
                        price: 1,
                    }),
                ],
            }),
        ];
        expect(await mapper.getOrdersByDate('1970-01-01')).toEqual(expectedArray);
    });

    it('should return a bestselling product', async () => {
        const expectedObject = {
            productName: 'Test',
            quantity: 1,
            totalPrice: 1,
        };
        expect(await mapper.getBestsellerByDate('1970-01-01')).toEqual(expectedObject);
    });

    it('should return an empty object if there was no orders for specific date', async () => {
        const expectedObject = {};
        expect(await mapper.getBestsellerByDate('1970-01-02')).toEqual(expectedObject);
    });

    it('should return a list of customers', async () => {
        const expectedArray = [
            {
                fullName: 'A B',
                total: 1,
            },
        ];
        expect(await mapper.getBestBuyersByDate('1970-01-01')).toEqual(expectedArray);
    });
});
