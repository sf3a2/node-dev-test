import {Injectable, Inject} from '@nestjs/common';
import {Repository} from './Repository';
import {Order} from '../Model/Order';
import {Customer} from '../Model/Customer';
import {Product} from '../Model/Product';

@Injectable()
export class OrderMapper {
    @Inject() repository: Repository;

    async getOrdersByDate(date: string): Promise<Order[]> {
        const [orders, products, customers] = await Promise.all([
            this.repository.fetchOrders(),
            this.repository.fetchProducts(),
            this.repository.fetchCustomers(),
        ]);
        const response: Order[] = [];
        orders
            .filter(order => order.createdAt === date)
            .forEach(order => {
                const newOrder = new Order({
                    number: order.number,
                    customer: new Customer(customers.find(customer => customer.id === order.customer)),
                    products: order.products.map(productId => {
                        return new Product(products.find(product => product.id === productId));
                    }),
                });
                response.push(newOrder);
            });

        return response;
    }

    async getBestsellerByDate(date: string): Promise<any> {
        const orders = await this.getOrdersByDate(date);
        const sold: object = {};
        let bestseller: string = '';

        if (!orders.length) {
            return {};
        }

        orders
            .forEach(order => {
                order.products.forEach(product => {
                    if (!sold[product.name]) {
                        sold[product.name] = {
                            amount: 0,
                            total: 0,
                        };
                        bestseller = product.name;
                    }
                    sold[product.name].amount++;
                    sold[product.name].total += product.price;
                    if (sold[bestseller].amount < sold[product.name].amount) {
                        bestseller = product.name;
                    }
                });
            });

        return {
            productName: bestseller,
            quantity: sold[bestseller].amount,
            totalPrice: Math.round(sold[bestseller].total * 100) / 100,
        };
    }

    async getBestBuyersByDate(date: string): Promise<any[]> {
        const orders = await this.getOrdersByDate(date);
        const customers: any[] = [];

        orders.forEach(order => {
            const matchedCustomer = customers.find(
                customer => customer.fullName === (order.customer.firstName + ' ' + order.customer.lastName),
            );
            if (!matchedCustomer) {
                customers.push({
                    total: order.products.map(product => product.price).reduce((a, b) => a + b, 0),
                    fullName: order.customer.firstName + ' ' + order.customer.lastName,
                });
            } else {
                matchedCustomer.total = Math.round(
                    (matchedCustomer.total + order.products.map(product => product.price).reduce((a, b) => a + b, 0)) * 100
                ) / 100;
            }
        });

        return customers.sort((a, b) => {
            if (a.total < b.total) {
                return 1;
            }
            if (b.total < a.total) {
                return -1;
            }

            return 0;
        });
    }
}
