export class Product {
    name: string;
    price: number;

    constructor(data: Partial<Product>) {
        this.name = data.name;
        this.price = data.price;
    }
}
