export class Customer {
    firstName: string;
    lastName: string;

    constructor(data: Partial<Customer>) {
        this.firstName = data.firstName;
        this.lastName = data.lastName;
    }
}
