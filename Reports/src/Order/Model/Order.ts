import {Customer} from './Customer';
import {Product} from './Product';

export class Order {
    number: string;
    customer: Customer;
    products: Product[];

    constructor(data: Partial<Order>) {
        this.number = data.number;
        this.customer = data.customer;
        this.products = data.products;
    }
}

