import * as request from 'supertest';
import {Test} from '@nestjs/testing';
import {INestApplication} from '@nestjs/common';
import {ReportModule} from './../src/Report/ReportModule';

describe('', () => {
    let app: INestApplication;

    beforeAll(async () => {
        const moduleFixture = await Test.createTestingModule({
            imports: [ReportModule],
        }).compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/ (GET)', () => {
        return request(app.getHttpServer())
            .get('/')
            .expect(404);
    });

    it('/report/product/:date (GET)', () => {
        return request(app.getHttpServer())
            .get('/report/product/2019-08-09')
            .expect(200);
    });

    it('/report/customers/:date (GET)', () => {
        return request(app.getHttpServer())
            .get('/report/customers/2019-08-09')
            .expect(200);
    });
});
